package Library;

import java.io.IOException;

public interface UserDao {

	public abstract void register(User user) throws IOException;
	
	
	public abstract boolean Login(String userName,String password);
}
