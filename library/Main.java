package Library;

import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) throws IOException {

		
		Scanner sc = new Scanner(System.in);
		System.out.println("欢迎来到图书馆借阅中心");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		userLogin use = new userLogin();

		while (true) {

			System.out.println("现在请登录你的个人账户以便选择服务");
			System.out.println("1.注册");
			System.out.println("2.登录");
			System.out.println("3.退出");
			String choice = sc.nextLine();
			int temp = 0;
			switch (choice) {
			case "1":
				System.out.println("您选择了注册账户，现在请输入您需要注册的信息");
				System.out.println("请输入您的用户名");
				String userName = sc.nextLine();
				System.out.println("请输入您的密码");
				String password = sc.nextLine();

				User user = new User(userName, password);
				use.register(user);

				System.out.println("注册成功");
				break;

			case "2":
				System.out.println("您选择了登录账户，现在请输入您需要注册的信息");
				System.out.println("请输入您的用户名");
				String inuserName = sc.nextLine();
				System.out.println("请输入您的密码");
				String inpassword = sc.nextLine();

				if (use.Login(inuserName, inpassword)) {
					System.out.println("登陆成功");
					temp = 1;
					bookshelf.readFile();
				} 
				else{
					System.out.println("登录失败");
				}
				break;

			case "3":
				System.exit(0);
				break;
			}

			if (temp == 1){
				break;
			}
		}

	}
}
